# CentOS6 Vagrant Box提供(VirtualBox向け)

## 提供バージョン

- 6.9 [CentOS-6.9-x86_64-minimal.iso]

## 前提条件

下記がインストールされている事

- Vagrant ( https://www.vagrantup.com/downloads.html )  
- VirtualBox ( https://www.virtualbox.org/wiki/Downloads )

※古いバージョンでは動かない場合があります。

作成時に使用したバージョン

- Vagrant 1.9.3 (Windows 64-bit)  
- VirtualBox 5.1.22 (Windows)

## 使用方法(例)

Windowsコマンドプロンプト/Mac・Linuxターミナル  
$ `vagrant box add CentOS6 https://gitlab.com/nightonlypj/vagrant-box-centos6/raw/master/CentOS6.9.box`  
$ `vagrant init CentOS6`  
$ `vagrant up`

※[CentOS6 Vagrantfile＋Ansible playbook提供](https://gitlab.com/nightonlypj/vagrant-ansible-centos6)でも使用しています。
